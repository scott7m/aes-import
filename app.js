'use strict';

var fs = require('fs'),
	util = require('util'),
	xml2js = require('xml2js');

var parser = new xml2js.Parser({explicitArray: false});

// Process File
var file = process.argv[2];
var table = file.split(".")[0];
var outputFile = "/home/scottm/aes/sql/" + table + ".sql";
var dir = "/home/scottm/aes/AES-Export/AES-XML-Data/";
// console.log("Converting files in folder: " + dir);

// Initialize file
fs.writeFileSync(outputFile, "");
console.log("Output file initialized: " + outputFile);

console.log("Reading file: " + file);

// Read the data
fs.readFile(dir+file, 'utf-8', function(err, data){

	// Parse XML into objects
	parser.parseString(data, function(err, result){
		if (err) throw err;
		var row = result.RESULTS.ROW;

		for (var obj in row) {
			for (var obj2 in row[obj]) {
				var tupleArr = row[obj][obj2];
				// console.dir(tupleArr);
				var fields = [];
				var values = [];

				for (var j in tupleArr) {
					var fieldPushed = false;

					try {
						fields.push(tupleArr[j].$.NAME);
						fieldPushed = true;
					} catch (err) {
						console.log("Bad data: " + file + " Val: " + tupleArr[j]._)
						console.log(err);
					}

					var tupleVal = tupleArr[j]._ || "";

					if ((tupleVal != null) && (tupleVal.indexOf("-") > 0)) {
						tupleVal = fixDate(tupleVal);
					}
					// Only push value if there is a matching field name.
					if (fieldPushed) values.push(tupleVal);
				}

				// Only make a SQL line if there is a field pushed.
				if (fields.length > 0) {
					var line = makeSQL(table, fields, values);
					// console.log(line);
					fs.appendFileSync(outputFile, line);
				}
			}
		}
	});
});

var fixDate = function(badDate) {
	var months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];

	var dateSplit = badDate.split("-");
	
	try {
		var yearNum = Number(dateSplit[2]);
		var monthNum= months.indexOf(dateSplit[1]);
		var month = monthNum < 10 ? "0" + String(monthNum + 1) : String(monthNum + 1);
	} catch(err) {
		return badDate;
	}

	if (! yearNum) {
		return badDate;
	}

	var year;
	if (dateSplit[2] > 50) {
		year = "19" + dateSplit[2];
	} else {
		year = "20" + dateSplit[2];
	}

	var fixedDate = year + "-" + month + "-" + dateSplit[0];
	// console.log("Fixed date: " + badDate + " to be: " + fixedDate);

	return fixedDate;
};

var makeSQL = function(file, fields, values) {
	var fieldString = "", valueString = "";

	for (var i = 0; i < fields.length; i++) {
		fieldString += fields[i];
		if (i < fields.length - 1) {
			fieldString += ", ";
		}
	}

	for (var i = 0; i < values.length; i++) {
		var escapedValue = values[i].replaceAll("\"", "'");
		valueString += "\"" + escapedValue + "\"";
		if (i < values.length - 1) {
			valueString += ", ";
		}
	}

	var retVal = "INSERT INTO " + file + " (" +
		fieldString +") " +
		"VALUES (" + valueString + ");\n";

	return retVal;
};

String.prototype.replaceAll = function(find, replace) {
	var text = this;
	var index = text.indexOf(find);

	while (index != -1) {
		text = text.replace(find, replace);
		index = text.indexOf(find);
	}

	return text;
};
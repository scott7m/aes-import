'use strict';

// This should be run as follows:
// node --max-old-space-size=4096 index.js


var fs = require('fs'),
    util = require('util'),
    xml2js = require('xml2js');

var parser = new xml2js.Parser({
    explicitArray: false
});

var outputFile = "/home/scottm/aes-mysql.sql";
var dir = "/home/scottm/aes/AES-Export/AES-XML-Data/";

console.log("Converting files in folder: " + dir);

// Initialize file
fs.writeFile(outputFile, "", function (err) {
    if (err) {
        return console.log(err);
    }

    console.log("Output file initialized: " + outputFile);
});

// Get Directory listing

fs.readdir(dir, function (err, files) {
    if (err) {
        throw err;
    }

    var table;
    var insertWritten;

    // Spin through files
    files.forEach(function (file) {
        // console.log('Processing file: ' + file);

        table = file.split(".")[0];
        insertWritten = false;

        // Read the data
        fs.readFile(dir + file, 'utf-8', function (err, data) {
            console.log('About to process data: ' + data.substr(0, 200) + '...');

            // Parse XML into objects
            parser.parseString(data, function (err, result) {
                if (err) throw err;
                var row = result.RESULTS.ROW;

                for (var obj in row) {
                    for (var obj2 in row[obj]) {
                        var tupleArr = row[obj][obj2];
                        // console.dir(tupleArr);
                        var fields = [];
                        var values = [];

                        for (var j in tupleArr) {
                            var fieldPushed = false;

                            try {
                                fields.push(tupleArr[j].$.NAME);
                                fieldPushed = true;
                            } catch (err) {
                                console.log("Bad data: " + file + " Val: " + tupleArr[j]._)
                                console.log(err);
                            }

                            // Only push value if there is a matching field name.
                            if (fieldPushed) values.push(tupleArr[j]._ || " ");
                        }

                        // Only make a SQL line if there is a field pushed.
                        if (fields.length > 0) {
                            var line = makeInsert(file, fields) + makeValues(values) + ';';

                            // makeSQL(table, fields, values);
                            // console.log(line);

                            fs.appendFileSync(outputFile, line);
                        }
                    }
                }
            });
        });
    });
});

var makeInsert = function (file, fields) {
    var fieldString = "";

    for (var i = 0; i < fields.length; i++) {
        fieldString += fields[i];

        if (i < fields.length - 1) {
            fieldString += ", ";
        }
    }

    return "INSERT INTO " + file + " (" + fieldString + ") " + "VALUES ";
};

var makeValues = function (values) {
    var valueString = "";

    for (var i = 0; i < values.length; i++) {
        valueString += "'" + values[i] + "'";

        if (i < values.length - 1) {
            valueString += ", ";
        }
    }

    return "(" + valueString + ")";
};